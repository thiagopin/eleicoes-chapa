package br.uece.chapa.dao

import br.uece.chapa.dao.impl.AlunoDaoImpl
import br.uece.chapa.dao.impl.ChapaDaoImpl
import br.uece.chapa.dao.impl.VotoDaoImpl
import groovy.sql.Sql

class DaoFactory {

    private static final instance = new DaoFactory()
//    Sql sql = Sql.newInstance("jdbc:hsqldb:mem:database", "sa", "", "org.hsqldb.jdbcDriver")
    private Sql sql = Sql.newInstance("jdbc:hsqldb:file:/tmp/chapa", "sa", "", "org.hsqldb.jdbcDriver")

    private DaoFactory() {

    }

    public static DaoFactory getInstance() {
        return instance
    }

    public AlunoDao criarAlunoDao() {
        return new AlunoDaoImpl(sql)
    }

    public ChapaDao criarChapaDao() {
        return new ChapaDaoImpl(sql)
    }

    public VotoDao criarVotoDao() {
        return new VotoDaoImpl(sql)
    }
}
