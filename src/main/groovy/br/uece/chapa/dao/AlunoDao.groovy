package br.uece.chapa.dao

import br.uece.chapa.model.Aluno

interface AlunoDao {
    Aluno resgatarAluno(String login, String senha)

    Aluno resgatarAluno(Long id)

    List<Aluno> listaAlunos()

    void importarAlunos(InputStream is)
}