package br.uece.chapa.dao

import br.uece.chapa.model.Aluno
import br.uece.chapa.model.Chapa

interface ChapaDao {
    Long criarChapa(Chapa chapa)
    Long atualizarChapa(Chapa chapa)

    boolean chapaUnica(Chapa chapa)

    boolean chapaMembrosExclusivos(Chapa chapa)

    boolean chapaValida(Chapa chapa)

    boolean edicaoValida(Chapa chapa, Aluno aluno)

    boolean removerChapa(Chapa chapa)

    List<Chapa> listarChapas(Aluno aluno)

    List<Chapa> listarTodasChapas()

    Chapa resgatarChapa(Long id)
}