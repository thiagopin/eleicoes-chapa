package br.uece.chapa.dao.impl

import br.uece.chapa.dao.VotoDao
import br.uece.chapa.model.Aluno
import br.uece.chapa.model.Chapa
import br.uece.chapa.model.Voto
import groovy.sql.Sql
import gstorm.Gstorm
import gstorm.metadata.ClassMetaData

class VotoDaoImpl implements VotoDao {
    private Sql sql

    public VotoDaoImpl(Sql sql) {
        this.sql = sql
        new Gstorm(sql).createTableFor(new ClassMetaData(Voto))
    }

    public boolean votoAlunoExistente(Aluno aluno) {
        return !sql.rows("select * from voto where idaluno = ?", aluno.id).isEmpty()
    }

    public Long votarChapa(Voto voto) {
        sql.dataSet('VOTO').add(voto.toMap())
        return voto.id
    }

    public boolean removerVotoComChapa(Chapa chapa) {
        return sql.execute("delete from voto where idchapa = ?", chapa.id)
    }

    public Map<Chapa, Integer> contagemVotos() {
        sql.rows("select c.*, count(*) as votos from voto v left join chapa c on c.id = v.idchapa group by c.id order by c.nome")
                .collectEntries {
            [new Chapa(id: it.id, nome: it.nome, idpresidente: it.idpresidente,
                    idsecretario: it.idsecretario, idcriadapor: it.idcriadapor), it.votos]
        }
    }
}
