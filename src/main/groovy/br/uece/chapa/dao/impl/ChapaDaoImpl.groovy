package br.uece.chapa.dao.impl

import br.uece.chapa.dao.ChapaDao
import br.uece.chapa.model.Aluno
import br.uece.chapa.model.Chapa
import groovy.sql.Sql
import gstorm.Gstorm
import gstorm.metadata.ClassMetaData

class ChapaDaoImpl implements ChapaDao {

    private Sql sql

    private Closure<Chapa> transformarResultado = {
        new Chapa([it.keySet()*.toLowerCase(), it.values().toList()].transpose().collectEntries())
    }

    public ChapaDaoImpl(Sql sql) {
        this.sql = sql
        new Gstorm(sql).createTableFor(new ClassMetaData(Chapa))
    }

    public Long criarChapa(Chapa chapa) {
        sql.dataSet('CHAPA').add(chapa.toMap())
        chapa.id
    }

    public Long atualizarChapa(Chapa chapa) {
        sql.executeUpdate('update chapa set nome = ?, idpresidente = ?, idsecretario = ?, idtesoureiro = ?, idcriadapor = ? where id = ?',
        [chapa.nome, chapa.idpresidente, chapa.idsecretario, chapa.idtesoureiro, chapa.idcriadapor, chapa.id])
        chapa.id
    }

    public boolean chapaUnica(Chapa chapa) {
        String query = 'select * from chapa where nome = ?'
        if (chapa?.id) query += ' and id <> ?'
        List args = [chapa.nome] + (chapa?.id ? [chapa.id] : [])
        sql.rows(query, args).isEmpty()
    }

    public boolean chapaMembrosExclusivos(Chapa chapa) {
        String query = 'select * from chapa where (idpresidente in (?,?,?) or idsecretario in (?,?,?) or idtesoureiro in (?,?,?))'
        if (chapa?.id) query += ' and id <> ?'
        List args = ([chapa.idpresidente, chapa.idsecretario, chapa.idtesoureiro] * 3) + (chapa?.id ? [chapa.id] : [])
        sql.rows(query, args).isEmpty()
    }

    public boolean chapaValida(Chapa chapa) {
        chapaUnica(chapa) && chapaMembrosExclusivos(chapa)
    }

    public boolean edicaoValida(Chapa chapa, Aluno aluno) {
        chapa.criadaPor.id == aluno.id
    }

    public boolean removerChapa(Chapa chapa) {
        sql.execute("delete from chapa where id = ?", chapa.id)
    }

    public List<Chapa> listarChapas(Aluno aluno) {
        sql.rows('select * from chapa where idcriadapor = ?', aluno.id)
                .collect(transformarResultado)
    }

    public List<Chapa> listarTodasChapas() {
        sql.rows('select * from chapa').collect(transformarResultado)
    }

    public Chapa resgatarChapa(Long id) {
        sql.rows('select * from chapa where id = ?', id).findResult(transformarResultado)
    }
}
