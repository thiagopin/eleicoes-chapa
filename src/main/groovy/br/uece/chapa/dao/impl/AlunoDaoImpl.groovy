package br.uece.chapa.dao.impl

import br.uece.chapa.dao.AlunoDao
import br.uece.chapa.model.Aluno
import groovy.sql.Sql
import gstorm.Gstorm
import gstorm.metadata.ClassMetaData

class AlunoDaoImpl implements AlunoDao {
    private Sql sql

    private Closure<Aluno> transformarResultado = {
        new Aluno([it.keySet()*.toLowerCase(), it.values().toList()].transpose().collectEntries())
    }

    public AlunoDaoImpl(Sql sql) {
        this.sql = sql
        new Gstorm(sql).createTableFor(new ClassMetaData(Aluno))
    }

    public void importarAlunos(InputStream is) {
        List<String> headers = ['nome', 'matricula', 'curso', 'senha']
        is.text.trim().split('\n').tail().each {
            Map dados = [headers, it.split(',')].transpose().collectEntries()
            def dataset = sql.dataSet('ALUNO')
            if (sql.rows('select * from aluno where matricula = ?', dados.matricula).isEmpty())
                dataset.add(dados)
        }
    }

    public Aluno resgatarAluno(String login, String senha) {
        return sql.rows('select * from aluno where matricula = ? and senha = ?', login, senha)
                .findResult(transformarResultado)
    }

    public Aluno resgatarAluno(Long id) {
        return sql.rows('select * from aluno where id = ?', id).findResult(transformarResultado)
    }

    public List<Aluno> listaAlunos() {
        return sql.rows('select * from aluno').collect(transformarResultado)
    }

}
