package br.uece.chapa.dao

import br.uece.chapa.model.Aluno
import br.uece.chapa.model.Chapa
import br.uece.chapa.model.Voto

interface VotoDao {
    boolean votoAlunoExistente(Aluno aluno)

    Long votarChapa(Voto voto)

    Map<Chapa, Integer> contagemVotos()

    boolean removerVotoComChapa(Chapa chapa)
}