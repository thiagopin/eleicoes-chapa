package br.uece.chapa.controller

import br.uece.chapa.Config
import br.uece.chapa.dao.ChapaDao
import br.uece.chapa.dao.DaoFactory
import br.uece.chapa.dao.VotoDao
import br.uece.chapa.model.Chapa
import br.uece.chapa.model.Voto
import javafx.beans.value.ChangeListener
import javafx.beans.value.ObservableValue
import javafx.collections.FXCollections as FXC
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.ComboBox
import javafx.scene.control.Label
import javafx.scene.layout.Pane

class VotarController implements Initializable {

    private ChapaDao chapaDao = DaoFactory.instance.criarChapaDao()
    private VotoDao dao = DaoFactory.instance.criarVotoDao()
    private Voto voto

    @FXML
    Pane votarPane

    @FXML
    ComboBox<Chapa> comboChapa

    @FXML
    Label labelMensagem

    @FXML
    Label labelPresidente

    @FXML
    Label labelSecretario

    @FXML
    Label labelTesoureiro

    @FXML
    private void votar() {
        Chapa chapa = comboChapa.selectionModel.selectedItem
        if (chapa) {
            if (dao.votoAlunoExistente(Config.app.sessao)) {
                labelMensagem.text = 'Não é possivel votar novamente!'
            } else {
                Voto voto = new Voto(aluno: Config.app.sessao, chapa: chapa)
                dao.votarChapa(voto)
                labelMensagem.text = 'Voto efetuado com sucesso.'
            }
        } else {
            labelMensagem.text = 'Nenhuma Chapa selecionada!'
        }
    }

    private void povoarCombos(List<Chapa> chapas) {
        comboChapa.items = FXC.observableArrayList(chapas)
        [comboChapa].each {
            it.valueProperty().addListener(new ChangeListener<Chapa>() {
                @Override
                public void changed(ObservableValue ov, Chapa anterior, Chapa novo) {
                    labelPresidente.text = novo.presidente.toString()
                    labelSecretario.text = novo.secretario.toString()
                    labelTesoureiro.text = novo.tesoureiro.toString()
                }
            })
            it.selectionModel.selectFirst()
        }
    }

    @Override
    void initialize(URL location, ResourceBundle resources) {
        povoarCombos(chapaDao.listarTodasChapas())
    }
}
