package br.uece.chapa.controller

import br.uece.chapa.Config
import br.uece.chapa.dao.AlunoDao
import br.uece.chapa.dao.DaoFactory
import br.uece.chapa.model.Aluno
import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.PasswordField
import javafx.scene.control.TextField
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import javafx.scene.layout.Pane

class LoginController implements Initializable {

    private AlunoDao dao = DaoFactory.instance.criarAlunoDao()

    @FXML
    Pane loginPane

    @FXML
    TextField campoLogin

    @FXML
    PasswordField campoSenha

    @FXML
    Button botaoCancelar

    @FXML
    Button botaoLogin

    @FXML
    Label labelMensagem

    @FXML
    public void eventoCancelar(ActionEvent evt) {
        System.exit(0)
    }

    @FXML
    public void eventoEnter(KeyEvent evt) {
        if (evt.code == KeyCode.ENTER) logar()
    }

    @FXML
    public void eventoLogar(ActionEvent evt) {
        logar()
    }

    private void logar() {
        Aluno aluno = dao.resgatarAluno(campoLogin.text.trim(), campoSenha.text)
        if (aluno) {
            Config.app.sessao = aluno
            Config.app.render(loginPane.scene.window, 'main')
        } else {
            [campoLogin, campoSenha].each { it.clear() }
            campoLogin.requestFocus()
            labelMensagem.text = 'Aluno inválido, tente novamente!!'
        }
    }

    @Override
    void initialize(URL location, ResourceBundle resources) {

    }
}
