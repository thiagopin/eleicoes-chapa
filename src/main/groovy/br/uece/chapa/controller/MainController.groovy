package br.uece.chapa.controller

import br.uece.chapa.Config
import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.layout.Pane

class MainController implements Initializable {

    @FXML
    Pane mainPane

    @FXML
    Pane mainContent

    @FXML
    public void cadastrarChapa(ActionEvent evt) {
        Config.app.renderPane(mainContent, 'chapa')
    }

    @FXML
    public void votar(ActionEvent evt) {
        Config.app.renderPane(mainContent, 'votar')
    }

    @FXML
    public void visuzlizarVotos(ActionEvent evt) {
        Config.app.renderPane(mainContent, 'votos')
    }

    @FXML
    public void sair(ActionEvent evt) {
        Config.app.sessao = null
        Config.app.render(mainPane.scene.window, 'login')
    }

    @Override
    void initialize(URL location, ResourceBundle resources) {

    }
}
