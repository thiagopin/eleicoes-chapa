package br.uece.chapa.controller

import br.uece.chapa.dao.ChapaDao
import br.uece.chapa.dao.DaoFactory
import br.uece.chapa.dao.VotoDao
import br.uece.chapa.model.Chapa
import javafx.collections.FXCollections as FXC
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.chart.BarChart
import javafx.scene.chart.XYChart
import javafx.scene.control.TableView
import javafx.scene.control.cell.PropertyValueFactory
import javafx.scene.layout.Pane

class VotosController implements Initializable {

    private ChapaDao chapaDao = DaoFactory.instance.criarChapaDao()
    private VotoDao dao = DaoFactory.instance.criarVotoDao()

    @FXML
    Pane votosPane

    @FXML
    TableView tabelaVotos

    @FXML
    BarChart<String, Integer> graficoVotos

    private void povoarTabela(Map<Chapa, Integer> votos) {
        def props = ['nome', 'votos','porcentagem']
        [tabelaVotos.columns, props].transpose().each { c, p ->
            c.cellValueFactory = new PropertyValueFactory(p)
        }
        tabelaVotos.items = FXC.observableArrayList(votos.collect { new Dados(nome: it.key, votos: it.value,porcentagem:(it.value/totalVotos(votos))*100)})
    }

    private void povoarGrafico(Map<Chapa, Integer> votos) {
        XYChart.Series series = new XYChart.Series();

        int votostotal = totalVotos(votos)
        series.setName("Porcentagem % - (${votostotal})Voto(s) ");

        votos.each { k, v ->
            series.data.add(new XYChart.Data(k.nome, (v/votostotal)*100))
        }
        graficoVotos.data.addAll(series)
    }
    private int totalVotos(Map<Chapa, Integer> votos) {

        int votostotal = 0
        votos.each {
            k,v -> votostotal+=v
        }
        return votostotal

    }

    @Override
    void initialize(URL location, ResourceBundle resources) {
        Map votos = dao.contagemVotos()
        povoarTabela(votos)
        povoarGrafico(votos)
    }

    class Dados {
        String nome
        Integer votos,porcentagem
    }
}
