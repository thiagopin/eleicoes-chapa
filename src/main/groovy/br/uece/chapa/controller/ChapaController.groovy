package br.uece.chapa.controller

import br.uece.chapa.Config
import br.uece.chapa.dao.AlunoDao
import br.uece.chapa.dao.ChapaDao
import br.uece.chapa.dao.DaoFactory
import br.uece.chapa.dao.VotoDao
import br.uece.chapa.model.Aluno
import br.uece.chapa.model.Chapa
import javafx.collections.FXCollections as FXC
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.*
import javafx.scene.control.cell.PropertyValueFactory
import javafx.scene.layout.Pane

class ChapaController implements Initializable {

    private ChapaDao dao = DaoFactory.instance.criarChapaDao()
    private AlunoDao alunoDao = DaoFactory.instance.criarAlunoDao()
    private VotoDao votoDao = DaoFactory.instance.criarVotoDao()
    private Chapa chapa

    @FXML
    Pane chapaPane

    @FXML
    TextField campoNome

    @FXML
    ComboBox<Aluno> comboPresidente

    @FXML
    ComboBox<Aluno> comboSecretario

    @FXML
    ComboBox<Aluno> comboTesoureiro

    @FXML
    TableView tabelaChapa

    @FXML
    Label labelMensagem

    @FXML
    Button botaoRemover

    @FXML
    private void salvarChapa() {
        if (!(chapa?.id)) {
            chapa = new Chapa(nome: campoNome.text, presidente: comboPresidente.selectionModel.selectedItem,
                    secretario: comboSecretario.selectionModel.selectedItem,
                    tesoureiro: comboTesoureiro.selectionModel.selectedItem,
                    criadaPor: Config.app.sessao)
        } else {
            chapa.nome = campoNome.text
            chapa.presidente = comboPresidente.selectionModel.selectedItem
            chapa.secretario = comboSecretario.selectionModel.selectedItem
            chapa.tesoureiro = comboTesoureiro.selectionModel.selectedItem
            chapa.criadaPor = Config.app.sessao
        }
        if (dao.chapaUnica(chapa)) {
            if (chapa.isMembrosDistintos()) {
                if (dao.chapaMembrosExclusivos(chapa)) {
                    if (chapa.id) dao.atualizarChapa(chapa)
                    else dao.criarChapa(chapa)
                    limparFormulario()
                    tabelaChapa.items.clear()
                    tabelaChapa.items = FXC.observableArrayList(dao.listarChapas(Config.app.sessao))
                } else {
                    labelMensagem.text = 'Existe(m) membro(s) desta chapa que pertence(m) a outra!'
                }
            } else {
                labelMensagem.text = 'Um único membro não pode ter papeis diferentes!'
            }
        } else {
            labelMensagem.text = 'Já exista uma chapa com este nome!'
        }
    }

    @FXML
    private void removerChapa() {
        votoDao.removerVotoComChapa(chapa)
        dao.removerChapa(chapa)
        limparFormulario()
        tabelaChapa.items = FXC.observableArrayList(dao.listarChapas(Config.app.sessao))
    }

    @FXML
    private void limparFormulario() {
        botaoRemover.disable = true
        this.chapa = null
        labelMensagem.text = ''
        campoNome.text = ''
        [comboPresidente, comboSecretario, comboTesoureiro].each {
            it.selectionModel.selectFirst()
        }
    }

    private void preencherFormulario(Chapa chapa) {
        limparFormulario()
        botaoRemover.disable = false
        this.chapa = chapa
        campoNome.text = chapa.nome
        comboPresidente.selectionModel.select(chapa.presidente)
        comboSecretario.selectionModel.select(chapa.secretario)
        comboTesoureiro.selectionModel.select(chapa.tesoureiro)
    }

    private void povoarCombos(List<Aluno> alunos) {
        comboPresidente.items = FXC.observableArrayList(alunos)
        comboSecretario.items = FXC.observableArrayList(alunos)
        comboTesoureiro.items = FXC.observableArrayList(alunos)
        [comboPresidente, comboSecretario, comboTesoureiro].each {
            it.selectionModel.selectFirst()
        }
    }

    private void povoarTabela(List<Chapa> chapas) {
        def props = ['id', 'nome', 'presidente', 'secretario', 'tesoureiro']
        [tabelaChapa.columns, props].transpose().each { c, p ->
            c.cellValueFactory = new PropertyValueFactory(p)
        }
        tabelaChapa.items = FXC.observableArrayList(chapas)
        tabelaChapa.rowFactory = {
            TableRow<Chapa> row = new TableRow<>()
            row.onMouseClicked = { evt ->
                if (evt.getClickCount() == 2 && (!row.isEmpty())) {
                    preencherFormulario(row.item)
                }
            }
            row
        }
    }

    @Override
    void initialize(URL location, ResourceBundle resources) {
        povoarTabela(dao.listarChapas(Config.app.sessao))
        povoarCombos(alunoDao.listaAlunos())
    }
}
