package br.uece.chapa

import br.uece.chapa.model.Aluno
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.layout.Pane
import javafx.stage.Stage

class Config {
    private Map<String, Parent> routers = [login: "fx/login.fxml", main: "fx/main.fxml", chapa: "fx/chapa.fxml",
                                           votar: "fx/votar.fxml", votos: "fx/votos.fxml"].asImmutable()
    private static Config app = new Config()
    Aluno sessao

    private Config() {

    }

    public static Config getApp() {
        return app
    }

    public render(Stage state, String name) {
        Parent parent = FXMLLoader.load(getClass().classLoader.getResource(routers[name]))
        state.setScene(new Scene(parent))
        state.show()
    }

    public renderPane(Pane pane, String name) {
        Parent parent = FXMLLoader.load(getClass().classLoader.getResource(routers[name]))
        pane.children.clear()
        pane.children.add(parent)
    }
}
