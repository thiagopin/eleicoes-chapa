package br.uece.chapa.model

import br.uece.chapa.dao.DaoFactory
import gstorm.Id

class Voto {
    @Id
    Long id
    Long idaluno, idchapa

    void setAluno(Aluno aluno) {
        idaluno = aluno.id
    }

    Aluno getAluno() {
        DaoFactory.instance.criarAlunoDao().resgatarAluno(idaluno)
    }

    void setChapa(Chapa chapa) {
        idchapa = chapa.id
    }

    Chapa getChapa() {
        DaoFactory.instance.criarChapaDao().resgatarChapa(idchapa)
    }

    public Map toMap() {
        [idaluno: idaluno, idchapa: idchapa]
    }
}
