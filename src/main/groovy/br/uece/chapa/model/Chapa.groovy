package br.uece.chapa.model

import br.uece.chapa.dao.DaoFactory
import groovy.transform.EqualsAndHashCode
import groovy.transform.TupleConstructor
import gstorm.Id

@TupleConstructor
@EqualsAndHashCode(includes = ['nome'])
class Chapa {
    @Id
    Long id
    String nome
    Long idpresidente, idsecretario, idtesoureiro, idcriadapor

    void setPresidente(Aluno presidente) {
        idpresidente = presidente.id
    }

    Aluno getPresidente() {
        DaoFactory.instance.criarAlunoDao().resgatarAluno(idpresidente)
    }

    void setSecretario(Aluno secretario) {
        idsecretario = secretario.id
    }

    Aluno getSecretario() {
        DaoFactory.instance.criarAlunoDao().resgatarAluno(idsecretario)
    }

    void setTesoureiro(Aluno tesoureiro) {
        idtesoureiro = tesoureiro.id
    }

    Aluno getTesoureiro() {
        DaoFactory.instance.criarAlunoDao().resgatarAluno(idtesoureiro)
    }

    void setCriadaPor(Aluno criadaPor) {
        idcriadapor = criadaPor.id
    }

    Aluno getCriadaPor() {
        DaoFactory.instance.criarAlunoDao().resgatarAluno(idcriadapor)
    }

    public Map toMap() {
        [nome        : nome, idpresidente: idpresidente, idsecretario: idsecretario,
         idtesoureiro: idtesoureiro, idcriadapor: idcriadapor]
    }

    public String toString() {
        return nome
    }

    public isMembrosDistintos() {
        return !(presidente == secretario || presidente == tesoureiro || tesoureiro == secretario)
    }
}
