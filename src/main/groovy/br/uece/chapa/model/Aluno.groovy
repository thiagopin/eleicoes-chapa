package br.uece.chapa.model

import groovy.transform.EqualsAndHashCode
import groovy.transform.TupleConstructor
import gstorm.Id

@TupleConstructor
@EqualsAndHashCode(includes = ['matricula'])
class Aluno {
    @Id
    Long id
    String nome, matricula, curso, senha

    @Override
    String toString() {
        return "$nome - Mat: $matricula - $curso"
    }
}
