package br.uece.chapa

import br.uece.chapa.dao.DaoFactory
import javafx.application.Application
import javafx.stage.Stage

public class Main extends Application {

    public static void main(String[] args) {
        launch(Main.class, args);
    }

    @Override
    public void start(Stage stage) throws IOException {
        stage.title = "Sistema de Eleições de Diretória"
        //importar alunos do arquivo 'alunos.csv'
        InputStream is = getClass().classLoader.getResourceAsStream('alunos.csv')
        DaoFactory.instance.criarAlunoDao().importarAlunos(is)
        Config.app.render(stage, 'login')
    }
}
