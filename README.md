# Trabalho de Padrões de Projeto - NPC2

## Descrição 

O Diretório Central dos Estudantes (DCE) da UECE está realizando eleições para a nova
diretoria. Para tornar o processo mais seguro, eles estão contratando alunos da disciplina
de Padrões de Software da UECE para implementar um sistema de cadastro de chapas,
votação e visualização dos resultados. O sistema deve ter as seguintes funcionalidades:

* Todo usuário é aluno da UECE e, para acessar o sistema, ele deve entrar com seu
login e senha, que é sua matrícula e a senha que ele usa para acessar o Aluno
Online.
* Na tela principal o usuário pode escolher entre cadastrar uma nova chapa, votar
ou visualizar votos.
* Ao cadastrar a chapa, o usuário deve informar o nome da chapa, bem como nome,
matrícula e curso do presidente, secretário e tesoureiro da chapa. Não pode haver
mais de uma chapa com o mesmo nome e o mesmo aluno não pode participar de
mais uma chapa.
* É possível que a chapa seja alterada ou excluída, desde que isso seja feito pelo
mesmo usuário que a criou.
* Na tela de votação o usuário visualiza o nome e a composição das chapas e só
pode escolher uma delas. O usuário pode votar apenas uma vez.
* A visualização dos resultados pode ser feita de duas maneiras: em tabela e em
gráfico (de barras ou pizza). Na tabela deve ser mostrado o nome de cada chapa,
a quantidade de votos que cada uma recebeu e a porcentagem relativa de votos.
Na gráfico deve aparecer apenas o nome da chapa e a porcentagem de votos.

Seu sistema deve ser implementado utilizando pelo menos os seguintes padrões:
* MVC (crie seu próprio, não utilize nenhum framework pronto)
* DAO (para encapsular acesso à fonte de dados. Deve-se criar um DAO para cada
entidade persistente na base de dados)
* Factory (para que retorne apenas o DAO solicitado)
* Singleton (para que haja apenas uma fábrica de DAOs).

O sistema pode ser implementado em qualquer linguagem orientada a objetos e ser tanto
dektop, web, ou mobile

## Como executar

Para executar o trabalho, executar os seguintes comandos abaixo (o Java 8 é necessário para a execução do projeto)

    git clone https://thiagopin@bitbucket.org/thiagopin/eleicoes-chapa.git
    cd eleicoes-chapa
    ./gradlew run #(para linux e mac)
    gradlew.bat run #(para windows)